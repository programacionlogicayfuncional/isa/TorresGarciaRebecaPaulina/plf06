
(ns plf06.core
  (:gen-class)
  (:require [clojure.string :as string]))

(def alfabeto "aábcdeéfghiíjklmnñoópqrstuúüvwxyz")
(def simbolo "01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789")
(def caracteres (vector alfabeto (string/upper-case alfabeto) simbolo))


(defn buscar-caracter
  [x]
  (letfn [(f [entrada caracter]
            (if (empty? caracter) x
                (if (nil? (string/index-of (first caracter) entrada))
                  (f entrada (rest caracter))
                  (str (get (first caracter) (if (< (+ 13 (string/index-of (first caracter) x)) (count (first caracter)))
                                               (+ 13 (string/index-of (first caracter) x))
                                               (- (+ 13 (string/index-of (first caracter) x)) (count (first caracter)))))))))]
    (f x caracteres)))

(defn rot-13
  [texto]
  (letfn [(f [a b c]
            (if (>= c (count a)) b (str b (f a (buscar-caracter (str (get a c))) (inc c)))))]
    (f texto "" 0)))

(defn -main
  [& args]
  (if (empty? args)
    (println "Favor de ingresar una cadena")
    (println (rot-13 (apply str args)))))


